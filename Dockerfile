# Use the official Windows Server Core image
FROM mcr.microsoft.com/windows/servercore:ltsc2019

# Install necessary features for RDP
RUN powershell -Command \
    Install-WindowsFeature -Name RDS-RD-Server; \
    Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server' -Name 'fDenyTSConnections' -Value 0; \
    Enable-NetFirewallRule -DisplayGroup "Remote Desktop"

# Download and install ngrok
ADD https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-windows-amd64.zip /ngrok.zip
RUN powershell -Command \
    Expand-Archive -Path /ngrok.zip -DestinationPath /ngrok; \
    Remove-Item /ngrok.zip

# Set ngrok authtoken
ENV NGROK_AUTH_TOKEN=2Nvl9INkYFRfl3uLSf3sU7PaAAL_4yoNusPRs3YbTpqcyuEdr

# Start ngrok and RDP
CMD powershell -Command \
    ./ngrok/ngrok authtoken $env:NGROK_AUTH_TOKEN; \
    Start-Process -NoNewWindow -FilePath ./ngrok/ngrok -ArgumentList 'tcp', '3389'; \
    Start-Sleep -Seconds 21600